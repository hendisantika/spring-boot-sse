package com.hendisantika.springbootsse;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-sse
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-09
 * Time: 21:52
 * To change this template use File | Settings | File Templates.
 */
@Service
public class MemoryObserverJob {

    public final ApplicationEventPublisher eventPublisher;

    public MemoryObserverJob(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Scheduled(fixedRate = 1000)
    public void doSomething() {
        MemoryMXBean memBean = ManagementFactory.getMemoryMXBean();
        MemoryUsage heap = memBean.getHeapMemoryUsage();
        MemoryUsage nonHeap = memBean.getNonHeapMemoryUsage();

        MemoryInfo mi = new MemoryInfo(heap.getUsed(), nonHeap.getUsed());
        this.eventPublisher.publishEvent(mi);
    }

}