package com.hendisantika.springbootsse;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-sse
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-09
 * Time: 21:51
 * To change this template use File | Settings | File Templates.
 */
public class MemoryInfo {
    private final long heap;

    private final long nonHeap;

    private final long ts;

    public MemoryInfo(long heap, long nonHeap) {
        this.ts = System.currentTimeMillis();
        this.heap = heap;
        this.nonHeap = nonHeap;
    }

    public long getHeap() {
        return this.heap;
    }

    public long getNonHeap() {
        return this.nonHeap;
    }

    public long getTs() {
        return this.ts;
    }
}
